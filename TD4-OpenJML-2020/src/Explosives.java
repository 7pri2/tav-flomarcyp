 // Based on a B specification from Marie-Laure Potet.
 
import java.util.ArrayList;

public class Explosives{
    public int nb_inc = 0;
    public String [][] incomp = new String[50][2];
    public int nb_assign = 0;
    public String [][] assign = new String[30][2];
 
    /*@ public invariant // Prop 1
      @ (0 <= nb_inc && nb_inc < 50);
      @*/

    // Propriete 1: Le nombre d'incompatibilités est entre 0 et 49, et de ce 
    // fait, on assure un accès correct au tableau des incompatibilités.

    /*@ public invariant // Prop 2
      @ (0 <= nb_assign && nb_assign < 30);
      @*/

    // Propriete 2: Le nombre d'assignations est entre 0 et 29, et donc on 
    // assure un accès correct au tableau des assignations

    /*@ public invariant // Prop 3
      @ (\forall int i; 0 <= i && i < nb_inc; 
      @         incomp[i][0].startsWith("Prod") && incomp[i][1].startsWith("Prod"));
      @*/

    // Propriete 3: Chaque valeur stockée dans le tableau des 
    // incompatibilités est préfixée de "Prod".

    /*@ public invariant // Prop 4
      @ (\forall int i; 0 <= i && i < nb_assign; 
      @         assign[i][0].startsWith("Bat") && assign[i][1].startsWith("Prod"));
      @*/

    // Propriete 4: Chaque valeur stockée dans la colonne 1 des assignations
    // est préfixée de "Bat" et chaque entrée de la seconde colonne par 
    // "Prod". (On veut s'assurer que l'on assigne bien des produits à des 
    // bâtiments).

    /*@ public invariant // Prop 5
      @ (\forall int i; 0 <= i && i < nb_inc; !(incomp[i][0]).equals(incomp[i][1]));
      @*/

    // Propriete 5: Aucun produit n'est incompatible avec lui-même

    /*@ public invariant // Prop 6
      @ (\forall int i; 0 <= i && i < nb_inc; 
      @        (\exists int j; 0 <= j && j < nb_inc; 
      @           (incomp[i][0]).equals(incomp[j][1]) 
      @              && (incomp[j][0]).equals(incomp[i][1]))); 
      @*/

    // Propriete 6: Si un produit A est incompatible avec un produit B, alors
    // ce produit B est lui aussi incompatible avec le produit A. On s'assure
    // que la relation est symmetrique.

    /*@ public invariant // Prop 7
      @ (\forall int i; 0 <= i &&  i < nb_assign; 
      @     (\forall int j; 0 <= j && j < nb_assign; 
      @        (i != j && (assign[i][0]).equals(assign [j][0])) ==>
      @        (\forall int k; 0 <= k && k < nb_inc;
      @           (!(assign[i][1]).equals(incomp[k][0])) 
      @              || (!(assign[j][1]).equals(incomp[k][1])))));
      @*/

    // Propriete 7: Pour deux assignations à un même bâtiment, les deux 
    // produits ne sont pas incompatibles.

    /*@ public invariant // Prop 8
      @ (\forall int i; 0 <= i &&  i < nb_assign; 
      @     (\forall int j; 0 <= j && j < nb_assign; 
      @        (i != j ==> ! ((assign[i][0]).equals(assign [j][0]) && (assign[i][1]).equals(assign[j][1])))));
      @*/

    // Propriete 8: toutes les lignes du tableau des affectations (assign) 
    // sont différentes deux à deux. 
    
    /*@ public invariant // Prop 9
      @ (\forall int i; 0 <= i &&  i < nb_assign; 
      @     (\forall int j; 0 <= j && j < nb_assign; 
      @        (i != j ==> ! ((assign[i][0]).equals(assign [j][0]) && (assign[i][1]).equals(assign[j][1])))));
      @*/

    // Propriete 9: un produit ne peut pas être stocké dans plus de trois bâtiments.
    
    /*@ public invariant // Prop 10
      @ (\forall int i; 0 <= i && i < nb_assign;
      @     (\forall int j; 0 <= j && j < nb_assign;
      @         (i != j && (assign[i][1]).equals(assign[j][0]) ==> 
      @             !(\exists int k; 0 <= k && k < nb_assign;
      @                 (k != i && k != j && (assign[i][1]).equals(assign[k][1]))))));
      @*/

    // Propriete 10: le nombre d’incompatibilités (nb_inc) ne peut jamais diminuer.

    //@ requires prod1 != null;
    //@ requires prod2 != null;
    //@ requires nb_inc >= 0;
    //@ requires nb_inc < 48;
    //@ requires prod1.startsWith("Prod");
    //@ requires prod2.startsWith("Prod");
    //@ requires prod1 != prod2;
    //@ ensures \old(incomp.length) <= incomp.length;
    public void add_incomp(String prod1, String prod2){
        incomp[nb_inc][0] = prod1;
        incomp[nb_inc][1] = prod2;
        incomp[nb_inc+1][1] = prod1;
        incomp[nb_inc+1][0] = prod2;
        nb_inc = nb_inc+2;
    }
    
    //@ requires bat != null;
    //@ requires prod != null;
    //@ requires nb_assign >= 0;
    //@ requires nb_assign < 28;
    //@ requires bat.startsWith("Bat");
    //@ requires prod.startsWith("Prod");
    public void add_assign(String bat, String prod){
        assign[nb_assign][0] = bat;
        assign[nb_assign][1] = prod;
        nb_assign = nb_assign+1;
    }

    public void skip(){
    }

    // Méthode findBat:
    /* Pour un produit /prod passé en parametre
     * renvoit le premier batiment trouvé dans lequel il est possible de le stocker,
     * sans qu'il soit stocké avec des produits incompatibles.
     * S'il n'y a pas de bâtiment trouvé, la méthode renvoie ""
     * */
    /*@ requires prod != null;
    @ requires prod.startsWith("Prod");
    @ ensures  (\forall int i; 0 <= i && i < nb_assign;
    @ 			(assign[i][0].equals(\result) ==>
    @					compatible(prod,assign[i][1])));
    @ ensures \result.startsWith("Bat");
    @*/
    public String findBat(String prod) {
    	ArrayList<String> listBatVisited= new ArrayList<String>();
    	for(int i = 0;i<nb_assign;i++) {
    		String bat = assign[i][0];
    		if(!batVisited(bat, listBatVisited)) {
    			listBatVisited.add(bat);
    			if(compatibleBatiment(bat, prod)) {
    				return bat;
    			};
    		}
    	}
    	return "";
    }
    /* Cette fonction permet de vérifier si un batiment /bat a déjà été visité
     * dans le cadre de la recherche de la foonction findBat(),
     * donc s'il est présent dans /listeBatVisited
     */
    private boolean batVisited(String bat, ArrayList<String> listBatVisited) {
		for(String s : listBatVisited) {
			if (s.equals(bat)){
				return true;
			}
		}
		return false;
	}

    /* Cette fonction teste si pour un batiment /bat,
     * /prod est compatibles avec les produits assignés
     */
    private boolean compatibleBatiment(String bat, String prod){
    	boolean comp = true;
    	for(int i=0;i<nb_assign && comp;i++) {
    		if(assign[i][0].equals(bat)) {
    			comp=comp && compatible(prod, assign[i][1]);
    		}
    	}
    	return comp;
    }
    /* Teste la compatibilité entre 2 produits     *
     */
	public /*@ pure helper @*/   boolean compatible(String prod1, String prod2) {
    	boolean comp=true;
		for(int i = 0;i<nb_inc && comp;i++) {
    		comp= comp && !(incomp[i][0].equals(prod1) && incomp[i][1].equals(prod2));
    	}
    	return comp;
    }

}
