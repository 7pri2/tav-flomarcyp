import static org.junit.Assert.*;

import org.jmlspecs.utils.JmlAssertionError;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestExplosivesJUnit4 {

    static int nb_inconclusive = 0;
    static int nb_fail = 0;

    Explosives e;

    public static void main(String args[]) {
    	String testClass = "TestExplosivesJUnit4";
     	org.junit.runner.JUnitCore.main(testClass);
     }


    private void handleJMLAssertionError(JmlAssertionError e) {
    	if (e.getClass().equals(JmlAssertionError.PreconditionEntry.class)) {
    	    System.out.println("\n INCONCLUSIVE "+(new Exception().getStackTrace()[1].getMethodName())+ "\n\t "+ e.getMessage());
            nb_inconclusive++;}
    else{
	    // test failure	
	    nb_fail++;
	    fail("\n\t" + e.getMessage());	
		}  
    }
	
    
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		nb_inconclusive = 0;
		nb_fail = 0;
		org.jmlspecs.utils.Utils.useExceptions = true;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	     System.out.println("\n inconclusive tests: "+nb_inconclusive+" -- failures : "+nb_fail );
	}
	
	@Test
	public void  testSequence_0() {
		try{
			e=new Explosives();
			e.add_incomp("Prod_Nitro","Prod_Glycerine");
			e.add_incomp("Prod_Dyna","Prod_Mite");
			e.add_assign("Bat_1","Prod_Dyna");
			e.add_assign("Bat_1","Prod_Nitro");
			e.add_assign("Bat_2","Prod_Mite");
			e.add_assign("Bat_1","Prod_Glycerine");
		} 	catch(JmlAssertionError e){
				handleJMLAssertionError(e);		
		}  
	}

    @Test
    public void testProp1() {
        try {
            e = new Explosives();
            for(int i = 0; i < 51; ++i)
                e.add_incomp("Prod_"+String.valueOf(i), "Prod_"+String.valueOf(i+1));
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }

    @Test
    public void testProp2() {
        try {
            e = new Explosives();
            for(int i = 0; i < 31; ++i)
                e.add_assign("Bat_"+String.valueOf(i), "Prod_"+String.valueOf(i+1));
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }

    @Test
    public void testProp3() {
        try {
            e = new Explosives();
            e.add_incomp("Pas du tout", "de prefixe");
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }

    @Test
    public void testProp4() {
        try {
            e = new Explosives();
            e.add_assign("Pas du tout", "de prefixe");
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }

    @Test
    public void testProp5() {
        try {
            e = new Explosives();
            e.add_incomp("Prod_maladie_auto-immune", "Prod_maladie_auto-immune");
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }

    @Test
    public void testIncompNull() {
        try {
            e = new Explosives();
            e.add_incomp(null, null);
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }

    @Test
    public void testAssignNull() {
        try {
            e = new Explosives();
            e.add_assign(null, null);
        } catch(JmlAssertionError e) {
            handleJMLAssertionError(e);		
        }
    }
}
